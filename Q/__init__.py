#!/usr/share/env python

from flask import Flask,render_template
app = Flask(__name__)

@app.route('/')
def home(debug=True):
    return render_template('index.html')

@app.route('/about')
def about(debug=True):
    return render_template('about.html')

if __name__=='__main__':
    app.run(debug=True)

